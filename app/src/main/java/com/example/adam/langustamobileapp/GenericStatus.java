package com.example.adam.langustamobileapp;

public class GenericStatus<T> {
    private String status;
    private String additionalInfo;
    private T payload;

    public GenericStatus() {
        this(null, null, null);
    }

    public GenericStatus(String status, String additionalInfo, T payload) {
        this.status = status;
        this.additionalInfo = additionalInfo;
        this.payload = payload;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
