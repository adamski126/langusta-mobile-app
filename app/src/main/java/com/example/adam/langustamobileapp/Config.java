package com.example.adam.langustamobileapp;

public class Config {
    private static Config config;
    private String token;
    private String url;

    public static Config getInstance() {
        if(config == null) {
            config = new Config();
        }
        return config;
    }

    public String getToken() {
        return token;
    }

    public String getUlr() { return url; }

    public void setToken(String newToken) {
        this.token = newToken;
    }

    public void setUrl(String newUrl) { this.url = newUrl; }
}

