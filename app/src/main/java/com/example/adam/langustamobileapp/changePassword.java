package com.example.adam.langustamobileapp;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v4.app.Fragment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class changePassword extends Fragment {


    public changePassword() {
        // Required empty public constructor
    }

    View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        this.view = view;

        Button changeButton = (Button)view.findViewById(R.id.changeButton);

        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassword(view);
            }
        });

        return view;
    }

    public void changePassword(View view){




        new Thread(new Runnable() {

            HttpURLConnection httpURLConnection;

            @Override
            public void run() {

                try {

                    EditText oldPassword = (EditText)view.findViewById(R.id.oldPassword);
                    EditText newPassword = (EditText)view.findViewById(R.id.newPassword);
                    EditText repeatNewPassword = (EditText)view.findViewById(R.id.repeatNewPassword);

                    String inNewPassword = newPassword.getText().toString();
                    String inOldPassword = oldPassword.getText().toString();

                    if(newPassword.getText().toString().equals(repeatNewPassword.getText().toString())){

                        URL url = new URL("https://langusta.spajste.ovh/api/user");
                        httpURLConnection = (HttpURLConnection) url.openConnection();
                        httpURLConnection.setRequestMethod("PUT");
                        httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                        httpURLConnection.addRequestProperty("Authorization", "Bearer " + Config.getInstance().getToken());
                        httpURLConnection.setDoOutput(true);
                        httpURLConnection.connect();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
                        outputStreamWriter.write("currentPassword="+inOldPassword+"&newPassword="+inNewPassword);
                        outputStreamWriter.flush();


                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                        String inputLine;
                        StringBuffer stringBuffer = new StringBuffer();
                        while ((inputLine = bufferedReader.readLine()) != null) {
                            stringBuffer.append(inputLine);
                        }
                        String result = stringBuffer.toString();

                        System.out.println(result);

                        bufferedReader.close();
                    } else {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Toast.makeText(getContext(),"HASŁA NIE SĄ TAKIE SAME",Toast.LENGTH_SHORT).show();


                            }
                        });


                    }



                }catch (Exception e){

                    try {

                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                        String inputLine;
                        StringBuffer stringBuffer = new StringBuffer();
                        while ((inputLine = bufferedReader.readLine()) != null) {
                            stringBuffer.append(inputLine);
                        }
                        String result = stringBuffer.toString();
                        Log.e("GetPagesThread", result);

                    }catch (Exception e1){
                        e1.printStackTrace();
                    }

                    e.printStackTrace();
                }

            }
        }).start();

    }

}
