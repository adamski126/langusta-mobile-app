package com.example.adam.langustamobileapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.SQLOutput;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContestFragment extends Fragment {


    public ContestFragment() {
        // Required empty public constructor
    }


    View view;
    String fanpageId = "";
    String postId = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_contest, container, false);
        this.view = view;
        return view;
    }

    List<FacebookBasicPageInfo> fanpages = new ArrayList<>();
    Spinner spinner;
    Spinner postSpinner;
    String selectedFanpage;
    String selectedPost;
    Button createButton;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        createButton = (Button)view.findViewById(R.id.createContest);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createContest();

                System.out.println("CONTEST CREATED");
            }
        });

        new Thread(new Runnable() {
            HttpURLConnection httpURLConnection;




            @Override
            public void run() {
                try {
                    Gson gson = new Gson();

                    URL url = new URL("https://langusta.spajste.ovh/api/facebook/page/all");
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.addRequestProperty("Authorization", "Bearer "+Config.getInstance().getToken());
                    httpURLConnection.connect();

                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    String inputLine;
                    StringBuffer stringBuffer = new StringBuffer();
                    while((inputLine = bufferedReader.readLine()) != null) {
                        stringBuffer.append(inputLine);
                    }
                    String result = stringBuffer.toString();

                    bufferedReader.close();

                    Gson g = new Gson();

                    System.out.println("here1");

                    GenericStatus status = g.fromJson(result,GenericStatus.class);

                    JsonParser parser = new JsonParser();
                    Object obj = parser.parse(result);
                    JsonObject lev1 = (JsonObject)obj;


                    JsonElement payload = lev1.get("payload");
                    JsonObject fquery = payload.getAsJsonObject();


                    JsonElement accounts = fquery.get("accounts");
                    JsonObject paging = accounts.getAsJsonObject();

                    JsonArray array = paging.getAsJsonArray("data");

                    for(int i=0; i<array.size(); i++){

                        JsonObject page = array.get(i).getAsJsonObject();

                        String id = page.get("id").toString();
                        String name = page.get("name").toString();
                        Integer fanCount = Integer.valueOf(page.get("fan_count").toString());
                        boolean hasAddedApp  = Boolean.valueOf(page.get("has_added_app").toString());
                        String pageToken = page.get("page_token").toString();
                        String accessToken = page.get("access_token").toString();

                        fanpages.add(new FacebookBasicPageInfo(id,name,fanCount,hasAddedApp,pageToken,accessToken));

                    }

                    fanpages.forEach(e -> System.out.println("Name: " + e.getName() + " Id: " + e.getId()));


                    List<String> names = new ArrayList<>();

                    for(int i=0; i<array.size(); i++){
                        JsonObject page = array.get(i).getAsJsonObject();
                        names.add(page.get("name").toString());
                    }
                    System.out.println("Avaible fanpages:");
                    names.forEach(e -> System.out.println(e));

                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            spinner = (Spinner)view.findViewById(R.id.fanpageSpinner);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext() , android.R.layout.simple_spinner_dropdown_item, names);
                            spinner.setAdapter(adapter);
                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    selectedFanpage = spinner.getSelectedItem().toString();

                                    Log.e("Selected: ", selectedFanpage);



                                    getPostLink();

                                    for(FacebookBasicPageInfo fb : fanpages){
                                        if(fb.getName().equals(selectedFanpage)){
                                            fanpageId = fb.getId();
                                            System.out.println("FANPAGE ID: " + fanpageId);
                                        }
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                    });





                    //setSpinnerElements(names);



                    System.out.println("here2");


                } catch (final Exception e) {
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                        String inputLine;
                        StringBuffer stringBuffer = new StringBuffer();
                        while ((inputLine = bufferedReader.readLine()) != null) {
                            stringBuffer.append(inputLine);
                        }
                        String result = stringBuffer.toString();
                        Log.e("GetPagesThread", result);
                    } catch (Exception hindus) {
                        hindus.printStackTrace();
                    }
                    e.printStackTrace();
                }
                //Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
            }
        }).start();

    }

    public void getPostLink(){

        new Thread(new Runnable() {

            HttpURLConnection httpURLConnection;

            @Override
            public void run() {

                String postLinkUrl = "";

                postLinkUrl = "https://langusta.spajste.ovh/api/facebook/page/"+fanpageId.substring(1,fanpageId.length()-1)+"/posts";


                try{
                    URL url = new URL(postLinkUrl);
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.addRequestProperty("Authorization", "Bearer "+Config.getInstance().getToken());
                    httpURLConnection.connect();

                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    String inputLine;
                    StringBuffer stringBuffer = new StringBuffer();
                    while((inputLine = bufferedReader.readLine()) != null) {
                        stringBuffer.append(inputLine);
                    }
                    String result = stringBuffer.toString();

                    bufferedReader.close();

                    System.out.println(result);

                    JsonParser parser = new JsonParser();
                    Object obj = parser.parse(result);
                    JsonObject lev1 = (JsonObject)obj;


                    JsonElement payload = lev1.get("payload");
                    JsonObject fquery = payload.getAsJsonObject();


//                    JsonElement accounts = fquery.get("accounts");
//                    JsonObject paging = accounts.getAsJsonObject();

                    JsonArray array = fquery.getAsJsonArray("data");

                   // System.out.println(array.get(0));

                    List<FacebookBasicPostInfo> posts = new ArrayList<>();

                    for(int i=0; i<array.size(); i++){

                        JsonObject page = array.get(i).getAsJsonObject();
                        if(page != null) {
                            String createdTime = "?";
                            if(page.get("createdTime") != null)
                                createdTime = page.get("created_time").toString();

                            String content = "?";
                            if(page.get("message") != null)
                                content = page.get("message").toString();

                            String id = "?";
                            if (page.get("id") != null)
                                id = page.get("id").toString();

                            posts.add(new FacebookBasicPostInfo(createdTime, content, id));
                        }

                    }

                    List<String> postsOnFanpage = new ArrayList<>();

                    posts.forEach(e -> {
                        postsOnFanpage.add(e.getContent());
                    });



                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {



                            postSpinner = (Spinner)view.findViewById(R.id.postSpinner);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext() , android.R.layout.simple_spinner_dropdown_item, postsOnFanpage);
                            postSpinner.setAdapter(adapter);
                            postSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    selectedPost = postSpinner.getSelectedItem().toString();
                                    Log.e("Selected POST: ", selectedPost);

                                    for(FacebookBasicPostInfo post : posts){
                                        if(post.getContent().equals(selectedPost)){
                                            postId = post.getId();
                                            System.out.println("POST ID: " + postId);
                                        }
                                    }

                                    Thread.yield();


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });


                        }
                    });




                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }).start();


    }

    public void createContest(){

        EditText titleEditText;
        EditText endTimeEditText;

        titleEditText = (EditText)view.findViewById(R.id.titleEditText);
        endTimeEditText = (EditText)view.findViewById(R.id.endTimeEditText);



        new Thread(new Runnable() {

            HttpURLConnection httpURLConnection;

            @Override
            public void run() {

                String competitionUrl = "";

                competitionUrl = "https://langusta.spajste.ovh/api/contest";

                String title = titleEditText.getText().toString();
                String endTime = endTimeEditText.getText().toString();

                System.out.println("TITLE :" + title);
                System.out.println("END TIME: " + endTime);
                SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date endDate = new Date();
                try {
                    endDate = formatter.parse(endTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                System.out.println("UNIX DATE: " + endDate.getTime()/1000);

                //convert date to UTC+0 from UTC+1
                String unixDate = String.valueOf((endDate.getTime()/1000)-3600);

                String messageToWrite = "title="+title+"&platform=facebook&post_link="+postId.substring(1,postId.length()-1)+"&end_time="+unixDate;

                System.out.println("POSTID: "+postId);
                BufferedReader reader = null;

                try{
                    URL url = new URL(competitionUrl);
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.addRequestProperty("Authorization", "Bearer "+Config.getInstance().getToken());
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.connect();
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
                    outputStreamWriter.write(messageToWrite);
                    outputStreamWriter.flush();

                    InputStream stream = httpURLConnection.getInputStream();


                    reader = new BufferedReader(new InputStreamReader(stream));

                    StringBuffer buffer = new StringBuffer();
                    String line = "";

                    while ((line = reader.readLine()) != null) {
                        buffer.append(line+"\n");
                        Log.d("Response: ", "> " + line);

                    }
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    try {
                        if (reader != null) {
                            reader.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }
        }).start();

    }
}
