package com.example.adam.langustamobileapp;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {

    private class JsonTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... params) {


            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();


                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                    Log.d("Response: ", "> " + line);

                }

                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }



    public void login(View view){
        new Thread(new Runnable() {
            HttpURLConnection httpURLConnection;

            @Override
            public void run() {
                try {
                    Gson gson = new Gson();

                    EditText username = (EditText) findViewById(R.id.loginUsername);
                    EditText password = (EditText) findViewById(R.id.loginPassword);

                    URL url = new URL("https://langusta.spajste.ovh/api/login");
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    //httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.addRequestProperty("Authorization", "Bearer "+Config.getInstance().getToken());
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.connect();
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
                    outputStreamWriter.write("username="+username.getText().toString()+"&password="+password.getText().toString());
                    outputStreamWriter.flush();


                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    String inputLine;
                    StringBuffer stringBuffer = new StringBuffer();
                    while((inputLine = bufferedReader.readLine()) != null) {
                        stringBuffer.append(inputLine);
                    }
                    String result = stringBuffer.toString();

                    outputStreamWriter.close();
                    bufferedReader.close();


                    final LoginStatus loginStatus = gson.fromJson(result, LoginStatus.class);
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Log.d("LoginThread", loginStatus.getLoginState()+" "+loginStatus.getToken());
                            Config.getInstance().setToken(loginStatus.getToken());
                            changeActivity();
                        }
                    });
                } catch (final Exception e) {
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                        String inputLine;
                        StringBuffer stringBuffer = new StringBuffer();
                        while ((inputLine = bufferedReader.readLine()) != null) {
                            stringBuffer.append(inputLine);
                        }
                        String result = stringBuffer.toString();
                        Log.e("LoginThread", result);
                    } catch (Exception hindus) {
                        hindus.printStackTrace();
                    }
                    e.printStackTrace();
                }
                //Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
            }
        }).start();
    }

    public void changeActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void changeActivityRegister(View view){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

    }

}
