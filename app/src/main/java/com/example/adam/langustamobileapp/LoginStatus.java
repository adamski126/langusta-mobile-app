package com.example.adam.langustamobileapp;

public class LoginStatus {
    private String loginState;
    private Integer userId;
    private String token;

    public LoginStatus() {
        this(null,null,null);
    }

    public LoginStatus(String loginState, Integer userId, String token) {
        this.loginState = loginState;
        this.userId = userId;
        this.token = token;
    }

    public String getLoginState() {
        return loginState;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getToken() {
        return token;
    }

    public void setLoginState(String loginState) {
        this.loginState = loginState;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
