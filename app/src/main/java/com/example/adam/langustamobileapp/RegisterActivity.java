package com.example.adam.langustamobileapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class RegisterActivity extends AppCompatActivity {

    String errorMessage = "User already in use!";

    public void register(View view){
        new Thread(new Runnable() {
            HttpURLConnection httpURLConnection;
            TextView error = (TextView) findViewById(R.id.error);


            @Override
            public void run() {
                try {
                    Gson gson = new Gson();



                    Log.e("VISIBILITY BEFORE: ", String.valueOf(error.getVisibility()));

                    if(error.getVisibility() == View.VISIBLE){
                        error.setVisibility(View.INVISIBLE);
                       // Log.e("visibility:" , "LOLOLOLOLOLOLOLOLOLOL");
                    }


                    EditText email = (EditText) findViewById(R.id.registerEmail);
                    EditText password = (EditText) findViewById(R.id.registerPassword);
                    EditText firstName = (EditText) findViewById(R.id.registerFirstName);
                    EditText lastName = (EditText) findViewById(R.id.registerLastName);
                    EditText confirmPassword = (EditText) findViewById(R.id.registerConfirmPassword);
                    CheckBox registerAcceptRules = (CheckBox) findViewById(R.id.registerAcceptRules);

                    Log.e("EMAIL: ", email.getText().toString());
                    if(!(password.getText().toString().equals(confirmPassword.getText().toString()))){
                        Log.e("PASSWORD: " , "THROWN AN EXCEPTION");
                        Log.e("Password: ", password.getText().toString());
                        Log.e("Confirmed password: ", confirmPassword.getText().toString());
                        errorMessage = "!Password.equal(Confirm Password)";
                        throw new Exception();
                    }

                    if(!registerAcceptRules.isChecked()){
                        Log.e("CHECK BOX: ", "THROWN AN EXCEPTION");
                        errorMessage = "!registerAcceptRules.isChecked()";
                        throw new Exception();
                    }


                    URL url = new URL("https://langusta.spajste.ovh/api/register");
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.connect();
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
                    outputStreamWriter.write("email="+email.getText().toString()+"&pass="+password.getText().toString()+"&firstName="+firstName.getText().toString()+"&lastName="+lastName.getText().toString());
                    outputStreamWriter.flush();


                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    String inputLine;
                    StringBuffer stringBuffer = new StringBuffer();
                    while((inputLine = bufferedReader.readLine()) != null) {
                        stringBuffer.append(inputLine);
                    }
                    String result = stringBuffer.toString();

                    outputStreamWriter.close();
                    bufferedReader.close();
                    Log.d("LoginThread", "OK");
                    changeActivityLogin();
                } catch (final Exception e) {
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                        String inputLine;
                        StringBuffer stringBuffer = new StringBuffer();
                        while ((inputLine = bufferedReader.readLine()) != null) {
                            stringBuffer.append(inputLine);
                        }
                        String result = stringBuffer.toString();
                        Log.e("LoginThread", result);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                error.setVisibility(View.VISIBLE);
                                error.setText(errorMessage);
                                Log.e("VISIBILITY AFTER:", String.valueOf(error.getVisibility()));

                            }
                        });



                    } catch (Exception hindus) {
                        hindus.printStackTrace();
                    }
                    e.printStackTrace();
                }

            }
        }).start();
    }

    public void changeActivityLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);


    }
}
