package com.example.adam.langustamobileapp;

public class FacebookBasicPostInfo {

    String content;
    String createdTime;
    String id;

    public FacebookBasicPostInfo(String createdTime, String content, String id){
        this.createdTime = createdTime;
        this.content = content;
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public String getId() {
        return id;
    }
}
