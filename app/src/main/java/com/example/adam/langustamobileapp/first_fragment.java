package com.example.adam.langustamobileapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.JsonWriter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class first_fragment extends Fragment implements AdapterView.OnItemSelectedListener {


    public first_fragment() {
        // Required empty public constructor
    }

    private Context mContext;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
    }

    private View view;
    Spinner spinner;
    String selectedFanpage;
    List<FacebookBasicPageInfo> fanpages = new ArrayList<>();

    private void setSpinnerElements(List<String> elements) {
        spinner = (Spinner)view.findViewById(R.id.fanpageSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, elements);
        spinner.setAdapter(adapter);
    }


    public void sendPost(View view){
        new Thread(new Runnable() {
            HttpURLConnection httpURLConnection;


            @Override
            public void run() {
                try {
                    Gson gson = new Gson();
                    String fanpageId = "";

                    String registerUrl = "";
                    EditText postEditText = (EditText) view.findViewById(R.id.postEditText);

                    Log.e("POST Text: ", postEditText.getText().toString());

                    for(FacebookBasicPageInfo fb : fanpages){
                        if(fb.getName().equals(selectedFanpage)){
                            fanpageId = fb.getId();
                            System.out.println("FANPAGE ID: " + fanpageId);
                        }
                    }

                    FacebookFanpagePost newPost = new FacebookFanpagePost(postEditText.getText().toString());

                    String json = new Gson().toJson(newPost);


                    registerUrl = "https://langusta.spajste.ovh/api/facebook/page/" + fanpageId.substring(1,fanpageId.length()-1) + "/post";

                    URL url = new URL(registerUrl);
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.addRequestProperty("Authorization", "Bearer "+Config.getInstance().getToken());
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.connect();
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
                    outputStreamWriter.write("content="+postEditText.getText().toString());
                    outputStreamWriter.flush();


                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    String inputLine;
                    StringBuffer stringBuffer = new StringBuffer();
                    while((inputLine = bufferedReader.readLine()) != null) {
                        stringBuffer.append(inputLine);
                    }
                    String result = stringBuffer.toString();

                    outputStreamWriter.close();
                    bufferedReader.close();
                    Log.d("LoginThread", "OK");

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(),"SUCCESS",Toast.LENGTH_SHORT).show();
                        }
                    });

                } catch (final Exception e) {
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                        String inputLine;
                        StringBuffer stringBuffer = new StringBuffer();
                        while ((inputLine = bufferedReader.readLine()) != null) {
                            stringBuffer.append(inputLine);
                        }
                        String result = stringBuffer.toString();
                        Log.e("LoginThread", result);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {




                            }
                        });



                    } catch (Exception hindus) {
                        hindus.printStackTrace();
                    }
                    e.printStackTrace();
                }

            }
        }).start();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_first_fragment, container, false);
        this.view = view;

        new Thread(new Runnable() {
            HttpURLConnection httpURLConnection;




            @Override
            public void run() {
                try {
                    Gson gson = new Gson();

                    URL url = new URL("https://langusta.spajste.ovh/api/facebook/page/all");
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.addRequestProperty("Authorization", "Bearer "+Config.getInstance().getToken());
                    httpURLConnection.connect();

                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    String inputLine;
                    StringBuffer stringBuffer = new StringBuffer();
                    while((inputLine = bufferedReader.readLine()) != null) {
                        stringBuffer.append(inputLine);
                    }
                    String result = stringBuffer.toString();

                    bufferedReader.close();

                    Gson g = new Gson();

                    System.out.println("here1");

                    GenericStatus status = g.fromJson(result,GenericStatus.class);

                    JsonParser parser = new JsonParser();
                    Object obj = parser.parse(result);
                    JsonObject lev1 = (JsonObject)obj;


                    JsonElement payload = lev1.get("payload");
                    JsonObject fquery = payload.getAsJsonObject();


                    JsonElement accounts = fquery.get("accounts");
                    JsonObject paging = accounts.getAsJsonObject();

                    JsonArray array = paging.getAsJsonArray("data");

                    for(int i=0; i<array.size(); i++){

                        JsonObject page = array.get(i).getAsJsonObject();

                        String id = page.get("id").toString();
                        String name = page.get("name").toString();
                        Integer fanCount = Integer.valueOf(page.get("fan_count").toString());
                        boolean hasAddedApp  = Boolean.valueOf(page.get("has_added_app").toString());
                        String pageToken = page.get("page_token").toString();
                        String accessToken = page.get("access_token").toString();

                        fanpages.add(new FacebookBasicPageInfo(id,name,fanCount,hasAddedApp,pageToken,accessToken));

                    }

                    fanpages.forEach(e -> System.out.println("Name: " + e.getName() + " Id: " + e.getId()));


                    List<String> names = new ArrayList<>();

                    for(int i=0; i<array.size(); i++){
                        JsonObject page = array.get(i).getAsJsonObject();
                        names.add(page.get("name").toString());
                    }
                    System.out.println("Avaible fanpages:");
                    names.forEach(e -> System.out.println(e));


                    if(payload == null){
                        System.out.println("asfsadfasdfasdfsadf");
                    } else {
                        System.out.println("XDDD3" + payload);
                        //System.out.println(paging.get(1));
                    }

                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            Button postButton = (Button)view.findViewById(R.id.postButton);
                            postButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sendPost(view);
                                }
                            });

                            spinner = (Spinner)view.findViewById(R.id.fanpageSpinner);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext() , android.R.layout.simple_spinner_dropdown_item, names);
                            spinner.setAdapter(adapter);
                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    selectedFanpage = spinner.getSelectedItem().toString();

                                    Log.e("Selected: ", selectedFanpage);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                    });





                    //setSpinnerElements(names);



                    System.out.println("here2");


                } catch (final Exception e) {
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                        String inputLine;
                        StringBuffer stringBuffer = new StringBuffer();
                        while ((inputLine = bufferedReader.readLine()) != null) {
                            stringBuffer.append(inputLine);
                        }
                        String result = stringBuffer.toString();
                        Log.e("GetPagesThread", result);
                    } catch (Exception hindus) {
                        hindus.printStackTrace();
                    }
                    e.printStackTrace();
                }
                //Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
            }
        }).start();





        return view;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
